FROM alpine
RUN mkdir /opt
WORKDIR /opt
COPY git.sh /opt
RUN apk add -U --no-cache git
ENTRYPOINT ["./git.sh"]